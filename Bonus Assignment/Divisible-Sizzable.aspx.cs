﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Divisible_Sizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckNumber(object sender, EventArgs e)
        {
            int userValue = int.Parse(inputNumber.Text);

            bool flag = false;

            for (int i = 2; i <= userValue / 2; i++)
            {
                if (userValue % i == 0)
                {
                    flag = true;
                    break;
                }
            }
            if (flag == false)
            {
                output.InnerHtml = "Your no. " + userValue + " is a Prime Number";

            }
            else
            {
                output.InnerHtml = "Your no. " + userValue + " is not a prime number";
            }
            if (!Page.IsValid)
            {
                return;
            }
        }


    }
}
            
   