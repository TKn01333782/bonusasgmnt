﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="Bonus_Assignment.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Palindrome </h2>
    <h3>Lets check for Palindrome</h3>
    <br />
    <br />
    <label runat="server">Please Enter text in box </label><br />
    <asp:TextBox ID="palindrome1" runat="server" placeholder="text only"></asp:TextBox>
    <asp:RequiredFieldValidator ID="inputRequire" runat="server" ControlToValidate="palindrome1" ErrorMessage="Need some text" ForeColor="red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="inputExperssion" runat="server" ControlToValidate="palindrome1" ErrorMessage="Please enter valid text" ValidationExpression="[a-zA-Z ]+$" ForeColor="DarkMagenta"></asp:RegularExpressionValidator>
    <br />    
    <br />
    <asp:button ID="submit" runat="server" Text="check for palindrome" Onclick="Checkforpalindrome"/>
    <br />
    <br />

     <div id="output" runat="server"></div>
</asp:Content>
