﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_Assignment
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Checkforcartesian(object sender, EventArgs e)
        {
            double xValue = Convert.ToDouble(xAxis.Text);
            double yValue = Convert.ToDouble(yAxis.Text);

            if (xValue > 0 && yValue > 0)
            {
                output.InnerHtml = "Input " + xAxis.Text + " , " + yAxis.Text + " lies in Quadrant 1";
            }
            else if (xValue < 0 && yValue > 0)
            {
                output.InnerHtml = "Input " + xAxis.Text +" , "+ yAxis.Text + " lies in Quadrant 2";
            }
            else if (xValue > 0 && yValue > 0)
            {
                output.InnerHtml = "Input " + xAxis.Text + " , " + yAxis.Text + " lies in Quadrant 3";
            }
            else if (xValue > 0 && yValue < 0)
            {
                output.InnerHtml = "Input " + xAxis.Text + " , " + yAxis.Text + " lies in Quadrant 4";
            }

        }               
        protected void InputX_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(xAxis.Text) == 0)
            {
                args.IsValid = false;
            }

        }
        protected void InputY_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(yAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
    }
}