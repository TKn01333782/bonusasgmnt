﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Divisible-Sizzable.aspx.cs" Inherits="Bonus_Assignment.Divisible_Sizzable" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Divisible Sizzable </h2>
    <h3>Insert your values</h3>
    <div id="userInput">
        <label runat="server" >Lets Check that your input number is prime no.:</label>
        <asp:Textbox runat="server" ID="inputNumber" Placeholder="please put your value"></asp:Textbox>
        <asp:RequiredFieldValidator ID="inputRequire" runat="server" ControlToValidate="inputNumber" ErrorMessage="Please enter some value." ForeColor="DarkRed"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="inputRegularExperssion" runat="server" ControlToValidate="inputNumber" ErrorMessage="Please enter valid number." ValidationExpression="[0-9]+$" ForeColor="DarkBlue"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="sumbit" Text="Check your number?" runat="server" OnClick="CheckNumber" />
        
    </div>     
    <br />
    <br />

     <div id="output" runat="server"></div>
</asp:Content>

