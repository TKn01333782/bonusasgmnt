﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian-Smartesian.aspx.cs" Inherits="Bonus_Assignment.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Cartesian </h2>
    <h3>Please put some values in boxes</h3>
    <br /><br />
         
    <label runat="server">Please enter X value</label>
    <asp:TextBox ID="xAxis" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="inputX" runat="server" ControlToValidate="xAxis" ErrorMessage="Need some Text" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="inputX1" runat="server" ControlToValidate="xAxis" ErrorMessage="Please enter number only" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="DarkBlue" Display="Dynamic"></asp:RegularExpressionValidator>
    <asp:CustomValidator ID="userInputX" runat="server" ControlToValidate="xAxis" OnServerValidate="InputX_ServerValidate" ErrorMessage="Please don't enter Zero" ForeColor="Red"></asp:CustomValidator>
    <br />
    <label runat="server">Please enter Y value</label> 
    <asp:TextBox ID="yAxis" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="inputY" runat="server" ControlToValidate="yAxis" ErrorMessage="Need some Text" ForeColor="Red"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="inputY1" runat="server" ControlToValidate="yAxis" OnServerValidate="inputY_Servervalidate"   ErrorMessage="Please enter number only" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="DarkBlue"></asp:RegularExpressionValidator>
    <asp:CustomValidator ID="userInputY" runat="server" ControlToValidate="yAxis" OnServerValidate="InputY_ServerValidate" ErrorMessage="Please don't enter Zero" ForeColor="Red"></asp:CustomValidator>
    <br />

    <asp:button ID="submit" runat="server" Text="check your result" Onclick="Checkforcartesian"/>
    <br /><br />
    <div id="output" runat="server"></div>
</asp:Content>
